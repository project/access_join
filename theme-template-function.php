<?php

// This function needs to be placed within the template.php in your theme's folder
// Rename the function to the name of your theme, followed by underscore_blocks
//
// Example: garland_blocks

function YOURTHEMENAME_blocks($region) {
  $output = '';

  if ($list = access_join_block_list($region)) {
    foreach ($list as $key => $block) {
      // $key == <i>module</i>_<i>delta</i>
      $output .= theme('block', $block);
    }
  }

  // Add any content assigned to this region through drupal_set_content() calls.
  $output .= drupal_get_content($region);

  return $output;
}
?>