<?php

/**
*
* @file access_join.module
*
* This file controls the functions which handle merging access roles and requiring groups of them
*
*/

/**
* Set up the admin options
*
*/
function access_join_menu() {

  $items = array();
  $items['admin/user/access_join'] = array(
    'title' => 'Access Join',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('access_join_page', 1),
    'access arguments' => array('administer permissions'),
  );
  return $items;

}

/**
* Create the admin page
*
*/
function access_join_page() {

  drupal_set_title(t('Access Join'));

  $roles = user_roles();
  $form['admin_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select the admin roles which should not be joined'),
    '#collapsible' => FALSE,
    '#description' => t('Select the roles not to join with the selected user roles. These are usally your admin roles.'),
  );
  $form['admin_roles']['roles'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Select admin roles',
      '#options' => $roles,
      '#default_value' => variable_get('access_join_admin_roles',array()),
    );
  $form['non_inclusive_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select the roles of which only one will be required'),
    '#collapsible' => FALSE,
    '#description' => t('Select the roles which will not be joined, but one of which will be required. These are usually your global roles.'),
  );
  $form['non_inclusive_roles']['ni_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Select roles',
      '#options' => $roles,
      '#default_value' => variable_get('access_join_non_inclusive_roles',array()),
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 10,
  );
  return $form;

}

/**
* Save the selected roles
*
* @param mixed $form
* @param mixed $form_state
*/
function access_join_page_submit($form, &$form_state) {

  $save_roles = $form['submit']['#post']['roles'];
  $ni_save_roles = $form['submit']['#post']['ni_roles'];
  variable_set('access_join_admin_roles',$save_roles);
  variable_set('access_join_non_inclusive_roles',$ni_save_roles);
  drupal_set_message(t('Your changes have been saved.'));

}

/**
* Alter the Block and Content-Type and Node Access Control Forms
*
* @param mixed $form
* @param mixed $form_state
* @param mixed $form_id
*/
function access_join_form_alter(&$form, $form_state, $form_id) {

  // Add the selection to the block admin form
  if ($form_id == 'block_admin_configure') {

    // Get the block ID from the module and delta
    $bid = _access_join_get_block_bid($form['module']['#value'],$form['delta']['#value']);
    $form['role_vis_settings']['join_roles'] = array(
      '#type' => 'radios',
      '#title' => t('Require all selected roles to view block'),
      '#default_value' => variable_get('join_block_roles_'.$bid,0),
      '#options' => array('0'=>'No','1'=>'Yes'),
      '#description' => t('Selecting yes require the user to have all selected roles to view the block.'),
    );
    $form['#submit'][] = 'access_join_block_configure_submit';
  }

  // Add the selection to the content-type access control form
  if ($form_id == 'content_access_admin_settings') {
    $form['per_role']['clearer2'] = array(
      '#value' => '<br clear="all" />',
    );
    $form['per_role']['join_roles'] = array(
      '#type' => 'radios',
      '#title' => t('Require all selected roles to view content type'),
      '#default_value' => variable_get('join_content_type_roles_'.arg(3),0),
      '#options' => array('0'=>'No','1'=>'Yes'),
      '#description' => t('Selecting yes require the user to have all selected roles to view the content type.'),
    );
    $form['#submit'][] = 'access_join_content_type_configure_submit';
  }

  // Add the selction to the per node access control form
  if ($form_id == 'content_access_page') {
    $form['per_role']['clearer2'] = array(
      '#value' => '<br clear="all" />',
    );
    $form['per_role']['join_roles'] = array(
      '#type' => 'radios',
      '#title' => t('Require all selected roles to view this page'),
      '#default_value' => variable_get('join_node_roles_'.arg(1),0),
      '#options' => array('0'=>'No','1'=>'Yes'),
      '#description' => t('Selecting yes require the user to have all selected roles to view this page.'),
    );
    $form['#submit'][] = 'access_join_node_configure_submit';
  }
}

/**
* Save whether or not this block should fall under the purview of access_join
*
* @param mixed $form
* @param mixed $form_values
*/
function access_join_block_configure_submit($form, $form_values) {
  $bid = _access_join_get_block_bid($form['module']['#value'],$form['delta']['#value']);
  variable_set('join_block_roles_'.$bid,$form_values['values']['join_roles']);
}

/**
* Save whether a content_type falls under the purview of access_join
*
* @param mixed $form
* @param mixed $form_values
*/
function access_join_content_type_configure_submit($form, $form_values) {
  variable_set('join_content_type_roles_'.arg(3),$form_values['values']['join_roles']);
}

/**
* Save whether or not a node falls under the purview of access_join
*
* @param mixed $form
* @param mixed $form_values
*/
function access_join_node_configure_submit($form, $form_values) {
  variable_set('join_node_roles_'.arg(1),$form_values['values']['join_roles']);
}

/**
* Helper function to get the block id from the module and delta
*
* @param mixed $module
* @param mixed $delta
*/
function _access_join_get_block_bid($module,$delta) {
  $result = db_query("SELECT bid FROM {blocks} WHERE module = '%s' AND delta = '%s'",array($module,$delta));
  $bid = db_fetch_object($result);
  return $bid->bid;
}

// Node Access Function - Requires the Content Access Module

/**
* This function determines if the node should have its required roles joined,
* or if it should use the regular sql grant query
*
* @param mixed $grants
* @param mixed $nid
* @return mixed
*/
function access_join_create_grants_sql($grants, $nid) {

  // if this nid is not marked for joined roles, create the standard query and return
  if (!variable_get('join_node_roles_'.$nid,0)) {
    return $grants_sql = 'AND ('. implode(' OR ', $grants) .')';
  }

  // determine if there are any content access permissions
  $result = db_query("SELECT settings FROM {content_access} WHERE nid = %d",array($nid));
  $data = db_fetch_object($result);

  // if no content access permissions, create the standard query and return
  if (count($data) < 1) {
    return $grants_sql = 'AND ('. implode(' OR ', $grants) .')';
  }

  // We've passed the final point where we may return the standard sql grants query
  global $user;

  // Pull any admin roles that are to be ignored
  $ignored_roles = variable_get('access_join_admin_roles',array());

  // Pull any admin roles that are to be required, but not all will be required
  $ni_roles = variable_get('access_join_non_inclusive_roles',array());

  // Run through the roles and build an array of required roles
  $settings = unserialize($data->settings);
  foreach ($settings['view'] as $role) {
    if (!in_array($role,$ignored_roles)) {
      $req_roles[] = $role;
    }
  }

  // Run through the required roles and split out the non inclusive ones
  foreach ($req_roles as $arr_id => $role) {
    if (in_array($role,$ni_roles)) {
      $ni_req_roles[] = $role;
      unset($req_roles[$arr_id]);
    }
  }

  // Pull the user roles and determine if all required user roles are present
  $user_roles = array_keys($user->roles);
  $pass = true;
  foreach ($req_roles as $role) {
    if (!in_array($role,$user_roles)) {
      $pass = false;
    }
  }

  // Determine if any of the non-inclusive roles are matched
  if ($pass) {
    $role_match = false;
    foreach ($ni_req_roles as $role) {
      if (in_array($role,$user_roles)) {
        $role_match = true;
      }
    }
    if ($role_match == false) {
      $pass = false;
    }
  }

  if ($pass) {
    // Return a blank sql statement to ensure it will pass muster
    return '';
  } else {
    // Return an sql statement that will fail muster and deny access to the node.
    return " AND (gid = 999 AND realm = 'access_join_fail' )";
  }

}

// Block Access Functions -- Requires the Content Access Module

/**
 * Return all blocks in the specified region for the current user.
 *
 * * This function is only slightly modified from the block_list function *
 *
 * @param $region
 *   The name of a region.
 *
 * @return
 *   An array of block objects, indexed with <i>module</i>_<i>delta</i>.
 *   If you are displaying your blocks in one or two sidebars, you may check
 *   whether this array is empty to see how many columns are going to be
 *   displayed.
 *
 * @todo
 *   Now that the blocks table has a primary key, we should use that as the
 *   array key instead of <i>module</i>_<i>delta</i>.
 *
 * == MODIFIED FOR KASPERSKY ==
 * Allow the option to OR the blocks together!
 *
 */
function access_join_block_list($region) {
  global $user, $theme_key;

  static $blocks = array();

  if (!count($blocks)) {
    $rids = array_keys($user->roles);
    $result = db_query(db_rewrite_sql("SELECT DISTINCT b.* FROM {blocks} b LEFT JOIN {blocks_roles} r ON b.module = r.module AND b.delta = r.delta WHERE b.theme = '%s' AND b.status = 1 AND (r.rid IN (". db_placeholders($rids) .") OR r.rid IS NULL) ORDER BY b.region, b.weight, b.module", 'b', 'bid'), array_merge(array($theme_key), $rids));
    while ($block = db_fetch_object($result)) {
      if (!isset($blocks[$block->region])) {
        $blocks[$block->region] = array();
      }
      // Use the user's block visibility setting, if necessary
      if ($block->custom != 0) {
        if ($user->uid && isset($user->block[$block->module][$block->delta])) {
          $enabled = $user->block[$block->module][$block->delta];
        }
        else {
          $enabled = ($block->custom == 1);
        }
      }
      else {
        $enabled = TRUE;
      }

      // This is the portion that is different from the standard block_list function.
      // It adds another possiblity to the true/false assignment to the $enabled variable
      $enabled = access_join_block_roles($block,$enabled);

      // Match path if necessary
      if ($block->pages) {
        if ($block->visibility < 2) {
          $path = drupal_get_path_alias($_GET['q']);
          // Compare with the internal and path alias (if any).
          $page_match = drupal_match_path($path, $block->pages);
          if ($path != $_GET['q']) {
            $page_match = $page_match || drupal_match_path($_GET['q'], $block->pages);
          }
          // When $block->visibility has a value of 0, the block is displayed on
          // all pages except those listed in $block->pages. When set to 1, it
          // is displayed only on those pages listed in $block->pages.
          $page_match = !($block->visibility xor $page_match);
        }
        else {
          $page_match = drupal_eval($block->pages);
        }
      }
      else {
        $page_match = TRUE;
      }
      $block->enabled = $enabled;
      $block->page_match = $page_match;
      $blocks[$block->region]["{$block->module}_{$block->delta}"] = $block;
    }
  }

  // Create an empty array if there were no entries
  if (!isset($blocks[$region])) {
    $blocks[$region] = array();
  }

  foreach ($blocks[$region] as $key => $block) {
    // Render the block content if it has not been created already.
    if (!isset($block->content)) {
      // Erase the block from the static array - we'll put it back if it has content.
      unset($blocks[$region][$key]);
      if ($block->enabled && $block->page_match) {
        // Check the current throttle status and see if block should be displayed
        // based on server load.
        if (!($block->throttle && (module_invoke('throttle', 'status') > 0))) {
          // Try fetching the block from cache. Block caching is not compatible with
          // node_access modules. We also preserve the submission of forms in blocks,
          // by fetching from cache only if the request method is 'GET'.
          if (!count(module_implements('node_grants')) && $_SERVER['REQUEST_METHOD'] == 'GET' && ($cid = _block_get_cache_id($block)) && ($cache = cache_get($cid, 'cache_block'))) {
            $array = $cache->data;
          }
          else {
            $array = module_invoke($block->module, 'block', 'view', $block->delta);
            if (isset($cid)) {
              cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
            }
          }

          if (isset($array) && is_array($array)) {
            foreach ($array as $k => $v) {
              $block->$k = $v;
            }
          }
        }
        if (isset($block->content) && $block->content) {
          // Override default block title if a custom display title is present.
          if ($block->title) {
            // Check plain here to allow module generated titles to keep any markup.
            $block->subject = $block->title == '<none>' ? '' : check_plain($block->title);
          }
          if (!isset($block->subject)) {
            $block->subject = '';
          }
          $blocks[$block->region]["{$block->module}_{$block->delta}"] = $block;
        }
      }
    }
  }
  return $blocks[$region];
}

/**
* Check to see if this block should be viewed
*
* @param mixed $block
* @param boolean $enabled
*/
function access_join_block_roles($block,$enabled=false) {

  // Determine if this block is subject to role joining.
  // if not, return the existing status of the enabled $enabled variable
  if (!variable_get('join_block_roles_'.$block->bid,false)) {
    return $enabled;
  }

  // Pull any admin roles that are to be ignored
  $ignored_roles = variable_get('access_join_admin_roles',array());

  // Pull any admin roles that are to be required, but not all will be required
  $ni_roles = variable_get('access_join_non_inclusive_roles',array());

  // global that which we need.
  global $user, $theme_key;

  // Get this user's roles and then pull the roles from the block_roles table
  $rids = array_keys($user->roles);
  $result = db_query("SELECT DISTINCT r.rid FROM {blocks} b, {blocks_roles} r WHERE b.module = r.module AND b.delta = r.delta AND b.theme = '%s' AND b.bid = %d", array($theme_key, $block->bid));
  while ($rid = db_fetch_object($result)) {
    // build an array of the roles this block requires
    if (!in_array($rid->rid,$ignored_roles)) {
      $req_roles[] = $rid->rid;
    }
  }

  // Run through the required roles and split out the non inclusive ones
  foreach ($req_roles as $arr_id => $role) {
    if (in_array($role,$ni_roles)) {
      $ni_req_roles[] = $role;
      unset($req_roles[$arr_id]);
    }
  }

  // Set the default response to true
  $display = true;

  // Run through the required roles and set to false if the user does not have the role.
  foreach ($req_roles as $role) {
    if (!in_array($role,$rids)) {
      $display = false;
    }
  }

  if ($display) {
    $role_match = false;
    foreach ($ni_req_roles as $role) {
      if (in_array($role,$rids)) {
        $role_match = true;
      }
    }
    if ($role_match == false) {
      $display = false;
    }
  }

  // return whether or not to display the block
  return $display;
}
